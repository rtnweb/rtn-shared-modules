const {
  tailwindFlexGrid,
  tailwindTypography,
  tailwindUtilities
} = require('@rtn-partners/rtn-shared-tailwind-plugins')

module.exports = {
  theme: {
    colors: {
      transparent: 'transparent',
      current: 'currentColor',
      black: '#000000',
      white: '#FFFFFF',
      wash: '#f9f9f9', // Background wash
      primary: {
        100: '#b6ffa9', // Primary Light
        200: '#50d337', // Primary Medium
        300: '#2e841f' // Primary Dark
      },
      gray: {
        100: '#fcfcfc', // Off White
        200: '#edeef0', // Light Gray
        300: '#d1d1d1', // Medium Gray
        400: '#525252', // Dark Gray
        500: '#3a3a3a', // Charcoal
        600: '#1d1e1f', // Off Black
        700: '#111111'
      },
      status: {
        success: '#15964c',
        warning: '#e9a538',
        error: '#ef5248',
        'error-light': '#ffc1b2'
      }
    },
    fontFamily: {
      sans: ['Open Sans', 'sans-serif']
    },
    screens: {
      xs: '350px',
      'sm-down': { max: '575px' },
      sm: '576px',
      'md-down': { max: '767px' },
      md: '768px',
      'lg-down': { max: '991px' },
      lg: '992px',
      'xl-down': { max: '1199px' },
      xl: '1200px'
    },
    extend: {
      fontSize: {
        22: '1.375rem'
      },
      borderWidth: {
        3: '3px'
      },
      opacity: {
        5: '0.05',
        10: '.1'
      },
      spacing: {
        36: '9rem',
        'block-sm': '2.5rem', // 40px
        'block-md': '5rem', // 80px
        'block-lg': '10rem' // 160px
      },
      boxShadow: {
        md: '0 9px 25px 0 rgba(0,0,0,0.1)'
      },
      backgroundImage: (theme) => ({
        'gradient-white': 'linear-gradient(180deg, white, white)'
      }),
      keyframes: {
        fadeIn: {
          '0%': { opacity: 0 },
          '100%': { opacity: 1 }
        },
        fadeOut: {
          '100%': { opacity: 0 }
        }
      },
      animation: {
        'fade-in': 'fadeIn 0.3s ease-out forwards',
        'fade-out': 'fadeOut'
      }
    }
  },
  variants: {
    margin: ['responsive', 'last']
  },
  plugins: [
    tailwindFlexGrid({
      columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
      gutters: [5, 36],
      breakpoint: 768,
      maxWidth: 1600
    }),
    tailwindTypography(),
    tailwindUtilities()
  ]
}
