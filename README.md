# rtn-shared-modules

> Made with create-react-library

[![NPM](https://img.shields.io/npm/v/rtn-shared-modules.svg)](https://www.npmjs.com/package/rtn-shared-modules) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save @rtn-partners/rtn-shared-modules
```

## Usage

```jsx
import { Navigation, Footer } from '@rtn-partners/rtn-shared-modules'

function ExampleComponent({ props }) {
  return (
    <>
      <Navigation nav={props.nav} socialIcons={props.socialIcons} />
      <Footer footer={props.footer} socialIcons={props.socialIcons} />
    </>
  )
}
```

## Assumptions

- CSS reset from TailwindCSS is used
- Font file Open Sans is avaliable globally
- .is-fixed-body class is added to destination project
- Props from Sanity will be passed into the modules

For more information and an example of fetching props from Sanity see example app below:

## Constrains/Roadmap

- Using `A` tags rather than NextJSs `Link` tag to perform client-side transitions
- Add [@sanity/image-url](https://github.com/sanity-io/image-url) to provide image dimensions for more performant images

### Example app

Run the example app using Next JS 8

```
cd example
npm i
npm run dev
```

## Author

[JPrisk](https://github.com/JPrisk)

<hr>

## Development

```
git clone git@bitbucket.org:rtnweb/rtn-shared-modules.git
cd rtn-shared-modules
npm i
npm run start
```

### Publishing to NPM

Before publishing make sure to increment the `package.json`

```
npm publish
```

### Developing a module in context of another app

The following steps assume your repo is in the same parent working directory as a project using the module i.e.

```
.
/rtn-shared-modules
/rtn
```

To aviod multiple versions of react [(see issue)](https://github.com/transitive-bullshit/create-react-library#use-with-react-hooks)

```
cd rtn-shared-modules
npm i ../rtn/frontend/node_modules/react
```

navigate to main working project and uninstall package from npm

```
cd ../rtn
npm r @rtn-partners/rtn-shared-modules
```

re-install package from your local directory

```
npm i ../../rtn-shared-modules
```

start create-react-libraries dev server

```
cd ../rtn-shared-modules
npm start
```

Congratulations, you are now ready to start development. Just remember that when you commit your changes, do not stage the package.json files with relative links
