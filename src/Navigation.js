import React, { useState } from 'react'
import tw, { css, theme } from 'twin.macro'
import PropTypes from 'prop-types'
import Link from './utility/Link'
import { NAV_HEIGHT } from './constants'
import { bodyScroll, SearchIcon, GearIcon } from './util'
import NavigationTab from './NavigationTab'

const styledNavigation = css`
  ${tw`bg-black absolute z-20 w-full flex items-center`}
  height: ${NAV_HEIGHT}px;
`
const styledMenu = css`
  ${tw`overflow-y-scroll justify-between w-full left-0 transform transition-transform duration-300 ease-in-out fixed bg-black lg:translate-x-0 lg:static lg:flex`}
  top: ${NAV_HEIGHT}px;
  height: calc(100% - ${NAV_HEIGHT}px);
  transform: translateX(100%);
  padding-bottom: 105px;

  @media (min-width: ${theme`screens.lg`}) {
    ${tw`overflow-visible pb-0`}
    transform: inherit;
  }

  &.is-open {
    transform: translateX(0);
    &::-webkit-scrollbar,
    & *::-webkit-scrollbar {
      ${tw`hidden`}
    }
    &,
    & * {
      -ms-overflow-style: none; /* IE and Edge */
      scrollbar-width: none; /* Firefox */
    }
  }
`

const Navigation = ({
  nav: { image, navigationTabs, search, signIn },
  socialIcons: { socialIcons },
  loggedIn
}) => {
  const [navOpen, setNavOpen] = useState(false)
  const [isLoggedInLinksOpen, setIsLoggedInLinksOpen] = useState(false)

  const navToggle = () => {
    if (navOpen) {
      setNavOpen(false)
      bodyScroll.unlock()
    } else {
      setNavOpen(true)
      window.scrollTo(0, 0)
      bodyScroll.lock()
    }
  }

  return (
    <nav css={styledNavigation}>
      <div tw='wrapper'>
        <div tw='row h-full'>
          <div tw='col-12 static'>
            <div tw='flex justify-between'>
              <div css={styledMenu} className={navOpen ? 'is-open' : ''}>
                <div
                  css={[
                    tw`max-h-full overflow-y-scroll lg:overflow-visible lg:flex items-center justify-between w-full`
                  ]}
                >
                  <div tw='border-white border-opacity-25 border-t lg:border-0 w-full flex'>
                    <div tw='lg:mr-12'>
                      {image && (
                        <Link
                          href='/'
                          tw='hidden lg:block focus:browser-focus'
                          aria-label='Return to homepage'
                        >
                          <img
                            src={image.assetUrl}
                            alt={image.altText}
                            width='140'
                            height='29'
                          />
                        </Link>
                      )}
                    </div>
                    <div
                      role='menubar'
                      aria-orientation='horizontal'
                      tw='flex-1 lg:flex items-center'
                    >
                      {navigationTabs &&
                        navigationTabs.map((tab, i) => {
                          return (
                            <NavigationTab
                              key={tab._key}
                              tab={tab}
                              socialIcons={socialIcons}
                            />
                          )
                        })}
                      <div tw='flex flex-col px-5 mt-8 lg:mt-0 lg:flex-row lg:items-center pb-10 lg:p-0 lg:flex-grow lg:justify-end'>
                        {loggedIn && loggedIn.settingsUrl && (
                          <Link
                            role='menuitem'
                            tw='text-white mb-6 type-370 lg:hidden inline-block flex'
                            href={loggedIn.settingsUrl}
                          >
                            <GearIcon />
                            <span tw='ml-4 pb-2 border-b-3 border-primary-200'>
                              Account Settings
                            </span>
                          </Link>
                        )}
                        {search && search.url && (
                          <Link
                            role='menuitem'
                            href={search.url}
                            link={search}
                            css={[
                              tw`flex mr-6 pb-4 lg:pb-0 lg:m-0 focus:browser-no-focus focus-visible:browser-focus`,
                              css`
                                &:hover svg,
                                &:focus svg {
                                  ${tw`text-primary-200! hover:text-primary-200!`}
                                }
                              `
                            ]}
                          >
                            <SearchIcon />
                            <p tw='text-white border-b-3 border-primary-200 lg:sr-only ml-4 type-370'>
                              {search.label}
                            </p>
                          </Link>
                        )}

                        {!loggedIn && signIn && signIn.url && (
                          <Link
                            role='menuitem'
                            tw='mx-5 text-white type-240 focus:browser-no-focus focus-visible:browser-focus focus:text-primary-200 hover:text-primary-200 transition-colors duration-200 hidden lg:block'
                            href={signIn.url}
                            link={signIn}
                          >
                            {signIn.label}
                          </Link>
                        )}
                        {loggedIn &&
                          loggedIn.logoutUrl &&
                          loggedIn.settingsUrl && (
                            <button
                              tw='ml-8 mt-1 z-40 relative hidden lg:block'
                              role='menuitem'
                              onMouseEnter={() => {
                                setIsLoggedInLinksOpen(true)
                              }}
                              onMouseLeave={() => {
                                setIsLoggedInLinksOpen(false)
                              }}
                              onFocus={() => {
                                setIsLoggedInLinksOpen(true)
                              }}
                              onBlur={(e) => {
                                if (
                                  !e.relatedTarget ||
                                  !e.relatedTarget.href ||
                                  (e.relatedTarget.href.indexOf(
                                    loggedIn.logoutUrl
                                  ) === -1 &&
                                    e.relatedTarget.href.indexOf(
                                      loggedIn.settingsUrl
                                    ) === -1)
                                ) {
                                  setIsLoggedInLinksOpen(false)
                                }
                              }}
                            >
                              {isLoggedInLinksOpen && (
                                <div
                                  css={[
                                    tw`bg-white rounded-full p-2 absolute h-10 w-10 -top-2.5 -left-2.5`,
                                    css`
                                      z-index: -1;
                                    `
                                  ]}
                                />
                              )}
                              <div css='relative'>
                                <GearIcon dark={isLoggedInLinksOpen} />
                              </div>
                              {isLoggedInLinksOpen && (
                                <ul
                                  css={[
                                    tw`bg-black absolute w-56 pt-3 -right-1 top-7`,
                                    css`
                                      z-index: -2;
                                    `
                                  ]}
                                >
                                  <li>
                                    <Link
                                      role='menuitem'
                                      tw='px-5 py-4 border-white border-opacity-25 border-b text-left text-white type-1670 focus:browser-no-focus focus-visible:browser-focus focus:bg-gray-600 hover:bg-gray-600 focus:text-primary-200 hover:text-primary-200 transition-colors duration-300 ease-in-out hidden lg:block'
                                      href={loggedIn.logoutUrl}
                                    >
                                      Sign Out
                                    </Link>
                                  </li>
                                  <li>
                                    <Link
                                      role='menuitem'
                                      tw='px-5 pt-4 pb-5 text-left text-white type-1670 focus:browser-no-focus focus-visible:browser-focus focus:bg-gray-600 hover:bg-gray-600 transition-colors duration-300 focus:text-primary-200 hover:text-primary-200 ease-in-out hidden lg:block'
                                      href={loggedIn.settingsUrl}
                                    >
                                      Account Settings
                                    </Link>
                                  </li>
                                </ul>
                              )}
                            </button>
                          )}
                      </div>
                    </div>
                  </div>
                </div>
                {signIn && signIn.url && (
                  <div tw='px-5 pb-5 bottom-0 left-0 w-full absolute lg:hidden'>
                    {loggedIn && loggedIn.logoutUrl ? (
                      <Link
                        tw='text-white type-240 w-full lg-down:button-white'
                        href={loggedIn.logoutUrl}
                        link={loggedIn}
                        role='menuitem'
                      >
                        Sign Out
                      </Link>
                    ) : (
                      <Link
                        tw='text-white type-240 w-full lg-down:button-white'
                        href={signIn.url}
                        link={signIn}
                        role='menuitem'
                      >
                        {signIn.label}
                      </Link>
                    )}
                  </div>
                )}
              </div>
              {image && (
                <Link href='/' tw='lg:hidden' aria-label='Return to homepage'>
                  <img
                    src={image.assetUrl}
                    alt={image.altText}
                    width='140'
                    height='29'
                  />
                </Link>
              )}
              <button
                aria-haspopup='menu'
                type='button'
                onClick={navToggle}
                tw='lg:hidden'
                aria-expanded={navOpen}
              >
                {navOpen ? (
                  <svg
                    xmlns='http://www.w3.org/2000/svg'
                    width='25'
                    viewBox='0 0 512 512'
                  >
                    <title>Close</title>
                    <path
                      fill='#ffffff'
                      stroke='#ffffff'
                      strokeLinecap='round'
                      strokeLinejoin='round'
                      strokeWidth='32'
                      d='M368 368L144 144M368 144L144 368'
                    />
                  </svg>
                ) : (
                  <svg
                    width='25'
                    height='16'
                    viewBox='0 0 25 16'
                    xmlns='http://www.w3.org/2000/svg'
                  >
                    <title>Menu</title>
                    <g fill='#FFF' fillRule='evenodd'>
                      <path d='M0 0h25v1.5H0zM0 7h20v1.5H0zM0 14h15v1.5H0z' />
                    </g>
                  </svg>
                )}
              </button>
            </div>
          </div>
        </div>
      </div>
    </nav>
  )
}

Navigation.propTypes = {
  nav: PropTypes.shape({
    image: PropTypes.shape({
      altText: PropTypes.string,
      asset: PropTypes.object
    }),
    navigationTabs: PropTypes.arrayOf(PropTypes.object).isRequired,
    search: PropTypes.shape({
      label: PropTypes.string,
      url: PropTypes.string
    }),
    signIn: PropTypes.shape({
      label: PropTypes.string,
      url: PropTypes.string
    })
  }),
  socialIcons: PropTypes.object,
  loggedIn: PropTypes.oneOfType([
    PropTypes.shape({
      logoutUrl: PropTypes.string,
      settingsUrl: PropTypes.string
    }),
    PropTypes.bool
  ])
}

Navigation.defaultProps = {
  nav: {},
  socialIcons: {},
  loggedIn: false
}

export default Navigation
