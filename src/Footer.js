import React from 'react'
import PropTypes from 'prop-types'
import Link from './utility/Link'
import RichText from './utility/RichText'
import tw, { css } from 'twin.macro'

const Footer = ({ footer, socialIcons: { socialIcons } }) => {
  return (
    <footer tw='bg-black text-white type-1640 py-block-sm md:py-block-md'>
      <div tw='wrapper' aria-label='Footer'>
        <div tw='row'>
          <ul tw='col-12 sm:col-6 lg:col-2'>
            {footer.navigationItems?.length &&
              footer.navigationItems.map((item) => (
                <li key={item._key} tw='mb-4'>
                  <Link href={item.url} link={item} tw='inline-link'>
                    {item.label}
                  </Link>
                </li>
              ))}
          </ul>
          <div tw='col-12 sm:col-6 lg:col-3 mt-10 sm:mt-0'>
            <RichText
              className={css`
                * {
                  ${tw`type-1640!`}
                }
              `}
              content={footer.contact?.richText}
            />
          </div>
          <div tw='col-12 lg:col-7'>
            <p tw='type-2980 lg:type-tight-6080 lg:text-right mt-12 lg:-mt-4'>
              This is Roadtrip Nation.
            </p>
            <ul
              tw='flex lg:justify-end mt-10 lg:mt-12'
              aria-label='Connect With Us'
            >
              {!!socialIcons?.length &&
                socialIcons.map(
                  (icon) =>
                    icon?.lightIcon?.asset && (
                      <li tw='mr-6 lg:ml-6 lg:mr-0' key={icon._key}>
                        <Link
                          href={icon.pageUrl}
                          target='_blank'
                          rel='noopener noreferrer'
                          tw='cursor-pointer hover:opacity-50 ease-out duration-200 transition-opacity'
                        >
                          <img
                            src={icon.lightIcon.assetUrl}
                            alt={icon.lightIcon.altText}
                            width={35}
                            height={35}
                          />
                        </Link>
                      </li>
                    )
                )}
            </ul>
          </div>
        </div>
        <div tw='row'>
          <div tw='col-12 flex flex-col md:flex-row justify-between mt-10 md:mt-20'>
            <p tw='type-1640 md-down:order-2 sm-down:leading-tight '>
              Copyright &copy; <time>{new Date().getFullYear()}</time> Roadtrip
              Nation. All Rights Reserved.
            </p>
            <ul tw='flex md:justify-end md-down:order-1 mb-10'>
              {footer.navigationItemsSecondary?.length &&
                footer.navigationItemsSecondary.map((item) => (
                  <li key={item._key} tw='mr-4 md:ml-12 md:mr-0'>
                    <Link href={item.url} tw='inline-link'>
                      {item.label}
                    </Link>
                  </li>
                ))}
            </ul>
          </div>
        </div>
      </div>
    </footer>
  )
}
Footer.propTypes = {
  footer: PropTypes.shape({
    contact: PropTypes.object,
    navigationItems: PropTypes.array,
    navigationItemsSecondary: PropTypes.array
  }),
  socialIcons: PropTypes.shape({
    socialIcons: PropTypes.array
  })
}

Footer.defaultProps = {
  footer: {
    contact: {},
    navigationItems: [],
    navigationItemsSecondary: []
  },
  socialIcons: { socialIcons: [] }
}

export default Footer
