import tw from 'twin.macro'
import React from 'react'
/* eslint-disable */

const BlockRenderer = (props) => {
  return <p tw='type-1440 sm:type-2040 mb-3 sm:mb-4'>{props.children}</p>
}

const serializers = {
  marks: {
    link: ({ children, mark }) => {
      return (
        <a
          href={mark?.href}
          target={
            !!mark?.newTab
              ? '_blank'
              : undefined
          }
          rel={
            !!mark?.newTab
              ? 'noopener noreferrer'
              : undefined
          }
        >
          {children}
        </a>
      );
    },
  },
  types: {
    block: BlockRenderer
  }
}

export default serializers
