import React from 'react'
import PropTypes from 'prop-types'
import tw from 'twin.macro'

const Link = React.forwardRef((props, ref) => {
  const { link, children, className, ...other } = props
  return (
    <a
      ref={ref}
      css={className}
      target={!!link && !!link?.newTab ? '_blank' : ''}
      {...other}
    >
      {children}
    </a>
  )
})

Link.defaultProps = {
  className: '',
  link: {}
}

Link.propTypes = {
  className: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object,
    PropTypes.array
  ]),
  children: PropTypes.node.isRequired,
  href: PropTypes.string.isRequired,
  link: PropTypes.object
}

export default Link
