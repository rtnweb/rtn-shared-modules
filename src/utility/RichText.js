import BlockContent from '@sanity/block-content-to-react';
import PropTypes from 'prop-types';
import tw, { css } from 'twin.macro';
import serializers from './serializers';

const RichText = ({ content, className }) => {
  const RichTextStyles = css`
    ul,
    ol {
      ${tw`mb-4`}
    }
    a {
      ${tw`inline-link`}
    }
    ${tw`-mb-4`}
  `;
  return (
    <div css={[RichTextStyles, className]}>
      <BlockContent blocks={content} serializers={serializers} />
    </div>
  );
};

RichText.propTypes = {
  content: PropTypes.arrayOf(PropTypes.object),
  className: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object,
    PropTypes.array,
  ]),
};

RichText.defaultProps = {
  content: [],
  className: '',
};

export default RichText;
