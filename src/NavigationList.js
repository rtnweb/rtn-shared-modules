import React from 'react'
import tw, { css, styled, theme } from 'twin.macro'
import Link from './utility/Link'
import PropTypes from 'prop-types'

const NavList = styled.div`
  ${tw`flex`}
  ${({ horizontal }) =>
    horizontal ? tw`flex-col lg:flex-row` : tw`flex-col lg:mr-10`};
`

const NavGroup = styled.div`
  ${tw`flex`}
  ${({ horizontal }) =>
    horizontal ? tw`w-full lg:w-3/4` : tw`w-full flex-col lg:w-1/4`};
`

const ItemStyle = css`
  ${tw`text-left duration-200 ease-out mb-4 lg:w-full lg:mt-2 lg:p-2 focus:browser-no-focus focus-visible:browser-focus`}
  transition-property: background-color, border-width;
  @media (min-width: ${theme`screens.lg`}) {
    ${tw`-mx-2 border-b-2 border-transparent`}
    max-width: 268px;
    &:hover,
    &:focus {
      ${tw`bg-wash cursor-pointer border-primary-200`}
    }
  }
`

const NavigationList = ({
  navList: { text, navigationItems, horizontal },
  headingStyle
}) => {
  return (
    <NavGroup horizontal={horizontal}>
      <div tw='pt-6 lg:pt-0 w-full'>
        <p css={headingStyle}>{text}</p>
        <NavList horizontal={horizontal}>
          {navigationItems.map((navItem) => (
            <Link
              key={navItem._key}
              href={navItem.url[0].url}
              link={navItem.url[0]}
              css={[ItemStyle, horizontal && tw`lg:mr-10`]}
            >
              <p tw='text-sm font-bold mb-2 lg-down:text-primary-200 lg-down:inline-link'>{navItem.title}</p>
              <p tw='type-1240'>{navItem.url[0].label}</p>
            </Link>
          ))}
        </NavList>
      </div>
    </NavGroup>
  )
}

NavigationList.propTypes = {
  navList: PropTypes.shape({
    text: PropTypes.string,
    horizontal: PropTypes.bool,
    navigationItems: PropTypes.arrayOf(
      PropTypes.shape({
        title: PropTypes.string,
        url: PropTypes.array
      })
    )
  }),
  menu: PropTypes.object
}

NavigationList.defaultProps = {
  navList: {}
}

export default NavigationList
