import tw from 'twin.macro'

const Chevron = () => (
  <svg
    xmlns='http://www.w3.org/2000/svg'
    viewBox='0 0 512 512'
    width='25'
    height='12'
  >
    <path
      fill='none'
      stroke='currentColor'
      strokeLinecap='square'
      strokeMiterlimit='10'
      strokeWidth='48'
      d='M112 184l144 144 144-144'
    />
  </svg>
)

const SearchIcon = () => (
  <svg
    xmlns='http://www.w3.org/2000/svg'
    tw='stroke-current text-white transition-colors duration-200'
    width='25'
    height='25'
    viewBox='0 0 512 512'
  >
    <path
      d='M221.09 64a157.09 157.09 0 10157.09 157.09A157.1 157.1 0 00221.09 64z'
      strokeMiterlimit='10'
      strokeWidth='32'
    />
    <path
      strokeLinecap='round'
      strokeMiterlimit='10'
      strokeWidth='32'
      d='M338.29 338.29L448 448'
    />
  </svg>
)

const GearIcon = ({dark = false}) => (
  <svg width="25" height="25" viewBox="0 0 25 25" version="1.1" xmlns="http://www.w3.org/2000/svg">
    <g id="gear" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
        <g id="Regular-Icons" transform="translate(-739.000000, -454.000000)" fill={dark ? "#444444" : "#FFFFFF"}>
            <g id="Icons/regular/cog" transform="translate(737.000000, 452.000000)">
                <path d="M19.5944331,13 C19.6453939,12.7002438 19.6453939,12.3500978 19.6453939,12 C19.6453939,11.6499022 19.5944331,11.350093 19.5944331,11 L21.7418549,9.35008341 C21.9466597,9.20017885 21.9975724,8.95022572 21.8442573,8.70017644 L19.798661,5.24996755 C19.6963067,5.04977428 19.3896764,4.95020649 19.1848716,5.04977428 L16.6278882,6.04982716 C16.1164531,5.64992139 15.5031925,5.29977548 14.8894511,5.04977428 L14.5313792,2.39990577 C14.4804184,2.20019327 14.2756617,2 14.0199441,2 L9.92875158,2 C9.6730821,2 9.46832536,2.20019327 9.41736453,2.39990577 L9.00833182,5.04977428 C8.39454236,5.29977548 7.83219449,5.64987332 7.26936586,6.04982716 L4.71238255,5.04977428 C4.45666498,4.95020649 4.20094742,5.04977428 4.09859309,5.24996755 L2.05299683,8.70022452 C1.95112326,8.89988894 2.00198793,9.20017885 2.15535116,9.35013149 L4.35440681,11 C4.35440681,11.350093 4.30344598,11.6499022 4.30344598,12 C4.30344598,12.3500978 4.35440681,12.649907 4.35440681,13 L2.20693697,14.6499166 C2.00213216,14.7998212 1.95126749,15.0497743 2.10453456,15.2998236 L4.15013083,18.7500325 C4.25253324,18.9502257 4.55916355,19.0497935 4.76392028,18.9502257 L7.32095167,17.9501728 C7.83233872,18.3500786 8.44564741,18.7002245 9.05938879,18.9502257 L9.46842151,21.6000942 C9.51981502,21.8500954 9.72409099,22 9.97980856,22 L14.0710011,22 C14.3267186,22 14.5314754,21.7998067 14.5824362,21.6000942 L14.9919497,18.9502257 C15.6052584,18.7002245 16.168087,18.3501267 16.7303868,17.9501728 L19.2874182,18.9502257 C19.5430877,19.0497935 19.7988052,18.9502257 19.9012077,18.7500325 L21.9468039,15.2997755 C22.0491583,15.1001111 21.9977647,14.7998212 21.8444015,14.6498685 L19.5944331,13 Z M11.9743959,15.5000168 C9.97976048,15.5000168 8.39463851,13.9502498 8.39463851,12 C8.39463851,10.0497502 9.97976048,8.49998317 11.9743959,8.49998317 C13.9690314,8.49998317 15.5542014,10.0497502 15.5542014,12 C15.5542014,13.9502498 13.9690314,15.5000168 11.9743959,15.5000168 Z" id="Shape"></path>
            </g>
        </g>
    </g>
</svg>
)

/**
 * Lock the body using fixed positioning
 * This is needed for IOS <= 13.0 which
 * the body-scroll-lock package does not account for
 *
 */

let scrollPosition = 0

const bodyScroll = {
  lock() {
    if (typeof window !== 'undefined' || !window.document) {
      scrollPosition = window.pageYOffset
      document.body.classList.add('is-fixed-body')
      document.body.style.top = `-${scrollPosition}px`
    }
  },
  unlock() {
    if (typeof window !== 'undefined' || !window.document) {
      document.body.classList.remove('is-fixed-body')
      document.body.style.top = ''
      window.scrollTo(0, scrollPosition)
      scrollPosition = 0
    }
  }
}

export { bodyScroll, Chevron, SearchIcon, GearIcon }
