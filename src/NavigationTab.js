import React, { useRef, useEffect } from 'react'
import tw, { css, styled, theme } from 'twin.macro'
import PropTypes from 'prop-types'
import { useMenuState, Menu, MenuButton } from 'reakit/Menu'
import Link from './utility/Link'
import useWindowDimensions from './useWindowDimensions'
import { NAV_HEIGHT } from './constants'
import NavList from './NavigationList'
import { Chevron } from './util'

const NavTabItemsContainer = styled.div`
  ${tw`w-2/3 px-5 w-full lg:pt-12 lg:pb-20 lg:pl-10`}
`

const ChevronWrapper = styled.div`
  ${tw`transition-transform duration-200`}
  ${({ menuVisible }) =>
    menuVisible &&
    css`
      transform: rotate(180deg);
    `}
  margin: 5px 0;
`

const ActiveUnderline = styled.div`
  ${({ menuVisible, isDesktop }) =>
    menuVisible && isDesktop ? tw`block` : tw`hidden`};
  ${tw`w-full absolute h-1 bg-primary-200`}
  top: 56px;
`

const AnimatedMenu = css`
  ${tw`px-0 border-primary-200 w-full`}
  transition: opacity 0.5s ease-in-out;
  opacity: 0;
  will-change: opacity;
  [data-enter] & {
    opacity: 1;
    transition-delay: 0.2s;
  }
  [data-leave] & {
    transition: opacity 0.2s ease-in-out;
    opacity: 0;
  }
`

const AnimatedContent = css`
  transition: transform 0.3s ease-out;
  will-change: transform;
  transform: translateX(-20px);
  [data-enter] & {
    transition-delay: 0.2s;
    transform: translateX(0px);
  }
`

const NavBackground = css`
  ${tw`left-0 bottom-0 top-0 bg-wash absolute`}
`
const ListHeading = css`
  ${tw`text-lg font-bold pb-5 lg:pb-6 mb-5 border-b border-gray-500 lg:border-gray-200`}
  @media (min-width: ${theme`screens.lg`}) {
    ${tw`flex flex-col justify-end`}
    min-height: 73px;
  }
`

const NavigationTab = React.forwardRef((props, ref) => {
  const {
    tab: { heading, text, image, navigationLists, showSocialIcons },
    socialIcons,
    ...rest
  } = props
  const menu = useMenuState({ animated: 1, loop: true, wrap: true })
  const { windowWidth } = useWindowDimensions()
  const navBreakpoint = parseInt(theme`screens.lg`.replace(/^px+/i, ''), 10)
  const buttonRef = useRef();

  useEffect(() => {
    // Force mobile nav element
    // to position at top when clicked
    if (menu.visible) {
      buttonRef.current.focus()
    }
  }, [menu.visible])

  return (
    <>
      <MenuButton
        {...menu}
        ref={buttonRef}
        role='menuitem'
        css={[
          tw`text-white w-full type-1670 lg:mr-8 relative py-6 px-5 lg-down:(border-white border-opacity-25 border-b) overflow-visible lg:p-0 lg:w-auto focus:text-primary-200 hover:text-primary-200 transition-colors duration-200 focus:browser-no-focus focus-visible:browser-focus`,
          menu.visible
            ? css`
                ${tw`text-primary-200 lg-down:(border-b-4 border-primary-200)`}
                &:after {
                  @media (min-width: ${theme`screens.lg`}) {
                    ${tw`bg-primary-200 w-full absolute`}
                    content: '';
                    height: 4px;
                    bottom: -19px;
                    left: -5px;
                  }
                }
              `
            : tw`text-white`
        ]}
        {...rest}
      >
        <div tw='flex justify-between items-center'>
          <p tw='type-1670'>{heading}</p>
          <ChevronWrapper menuVisible={menu.visible}>
            <Chevron />
          </ChevronWrapper>
        </div>
        <ActiveUnderline
          menuVisible={menu.visible}
          isDesktop={windowWidth >= navBreakpoint}
        />
      </MenuButton>
      <Menu
        {...menu}
        preventBodyScroll
        css={[
          tw`w-full flex bg-black lg:bg-white z-10 lg:fixed browser-no-focus`,
          css`
            min-height: 375px;
            box-shadow: 0px 233px 129px 176px rgba(0, 0, 0, 0.55);
          `
        ]}
        style={{
          top: NAV_HEIGHT,
          transform: 'translate(0,0)',
          position: windowWidth >= navBreakpoint ? 'absolute' : 'static'
        }}
        aria-label={heading}
      >
        {windowWidth >= navBreakpoint && <div css={NavBackground} />}
        <div tw='lg:wrapper pb-0' css={AnimatedMenu}>
          <div tw='flex bg-black text-white lg:text-gray-500 lg:bg-white flex-col w-full h-full lg:flex-row'>
            <div tw='w-full flex flex-col justify-between bg-gray-600 lg:bg-wash lg:w-1/3 lg:my-6'>
              <div
                tw='w-full mx-auto lg:w-3/4 lg:flex lg:flex-col lg:justify-center h-full'
                css={AnimatedContent}
              >
                <p tw='py-5 px-5 type-2270 lg:type-2670 lg:px-0 lg:py-10'>
                  {text}
                </p>
              </div>
              {windowWidth >= navBreakpoint && (
                <img
                  alt={image.altText}
                  src={`${image.assetUrl}?h=340&w=760&q=75&fit=crop&crop=center`}
                  width='760'
                  height='340'
                  css={AnimatedContent}
                />
              )}
            </div>
            <NavTabItemsContainer>
              <div tw='flex flex-col lg:flex-row' css={AnimatedContent}>
                {navigationLists.map((navList) => (
                  <NavList
                    menu={menu}
                    key={navList._key}
                    navList={navList}
                    headingStyle={ListHeading}
                  />
                ))}
                {showSocialIcons && (
                  <div tw='flex'>
                    <div tw='w-full'>
                      <p css={ListHeading}>Connect With Us</p>
                      <div tw='flex mb-8 lg:m-0'>
                        {socialIcons.map((icon, i) => (
                          <Link
                            key={`nav-social-${i}`}
                            href={icon.pageUrl}
                            tw='cursor-pointer mr-4 lg:mr-3 transition-opacity duration-200 hover:opacity-50 focus:browser-no-focus focus-visible:browser-focus'
                            target='_blank'
                            rel='noopener noreferrer'
                          >
                            <img
                              src={icon.darkIcon.assetUrl}
                              alt={icon.darkIcon.altText}
                              width={35}
                              height={35}
                              tw='lg-down:hidden'
                            />
                            <img
                              src={icon.lightIcon.assetUrl}
                              alt={icon.lightIcon.altText}
                              width={35}
                              height={35}
                              tw='lg:hidden'
                            />
                          </Link>
                        ))}
                      </div>
                    </div>
                  </div>
                )}
              </div>
            </NavTabItemsContainer>
          </div>
        </div>
      </Menu>
    </>
  )
})

NavigationTab.propTypes = {
  tab: PropTypes.shape({
    heading: PropTypes.string,
    text: PropTypes.string,
    image: PropTypes.shape({
      altText: PropTypes.string,
      asset: PropTypes.object
    }),
    navigationLists: PropTypes.arrayOf(PropTypes.object),
    showSocialIcons: PropTypes.bool
  }),
  socialIcons: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string,
      pageUrl: PropTypes.string,
      lightIcon: PropTypes.shape({
        altText: PropTypes.string,
        asset: PropTypes.object
      }),
      darkIcon: PropTypes.shape({
        altText: PropTypes.string,
        asset: PropTypes.object
      })
    })
  )
}

NavigationTab.defaultProps = {
  tab: {},
  socialIcons: []
}

export default NavigationTab
