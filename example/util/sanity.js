import sanityClient from '@sanity/client'

const options = {
  // Find your project ID and dataset in `sanity.json` in your studio project
  dataset: process.env.dataset,
  projectId: process.env.projectId,
  useCdn: process.env.NODE_ENV === 'production'
  // useCdn == true gives fast, cheap responses using a globally distributed cache.
  // Set this to false if your application require the freshest possible
  // data always (potentially slightly slower and a bit more expensive).
}

const client = sanityClient(options)

// Preview is currently using the SSR preview setup https://www.sanity.io/docs/preview-content-on-site
// refer to docs for additional options
export const previewClient = sanityClient({
  ...options,
  useCdn: false,
  token: process.env.apiToken
})

export default client
