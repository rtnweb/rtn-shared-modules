import client, { previewClient } from './sanity'

const getClient = (preview) => (preview ? previewClient : client)

export async function getNav(preview) {
  const curClient = getClient(preview)
  const data = await curClient.fetch(`*[_type == "navigation"]{ 
    image{...,"assetUrl":asset->url},
    navigationTabs[]{...,image{...,"assetUrl":asset->url}},
    signIn,
    search
  }`)
  return data[0]
}

export async function getSocialIcons(preview) {
  const curClient = getClient(preview)
  const data = await curClient.fetch(
    `*[_type == "socialList"]{socialIcons[]{...,darkIcon{...,"assetUrl":asset->url},lightIcon{...,"assetUrl":asset->url}}}`
  )
  return data[0]
}

export async function getFooter(preview) {
  const curClient = getClient(preview)
  const data = await curClient.fetch(`*[_type == "footer"]{ 
    contact,
    navigationItems,
    navigationItemsSecondary
  }`)
  return data[0]
}
