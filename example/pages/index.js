import React from 'react'
import Head from 'next/head'

import {
  getNav,
  getSocialIcons,
  getFooter,
  getNewsletter
} from '../util/getGlobals'
import { Navigation, Footer } from '@rtn-partners/rtn-shared-modules'

function Home({ globals }) {
  return (
    <div>
      <Head>
        <title>Exampe Next 8 App</title>
        <meta name='viewport' content='initial-scale=1.0, width=device-width' />
        {/* Google Open Sans Font */}
        <link rel='preconnect' href='https://fonts.gstatic.com' />
        <link
          href='https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600;700;800&display=fallback'
          rel='stylesheet'
        />
        {/* Tailwind Reset */}
        <link
          rel='stylesheet'
          href='https://unpkg.com/tailwindcss@1.9.6/dist/base.css'
        />
      </Head>
      <style global jsx>{`
        .is-fixed-body {
          position: fixed;
          overflow: hidden;
          width: 100%;
        }
        .pattern {
          height: 600px;
          background-color: #269;
          background-image: linear-gradient(white 2px, transparent 2px),
            linear-gradient(90deg, white 2px, transparent 2px),
            linear-gradient(rgba(255, 255, 255, 0.3) 1px, transparent 1px),
            linear-gradient(
              90deg,
              rgba(255, 255, 255, 0.3) 1px,
              transparent 1px
            );
          background-size: 100px 100px, 100px 100px, 20px 20px, 20px 20px;
          background-position: -2px -2px, -2px -2px, -1px -1px, -1px -1px;
        }
      `}</style>
      <Navigation nav={globals.nav} socialIcons={globals.socialIcons} />
      <div className='pattern'></div>
      <Footer footer={globals.footer} socialIcons={globals.socialIcons} />
    </div>
  )
}

Home.getInitialProps = async ({ req }) => {
  let pageProps = {
    globals: {}
  }
  pageProps.globals.nav = await getNav(false)
  pageProps.globals.socialIcons = await getSocialIcons(false)
  pageProps.globals.footer = await getFooter(false)

  return { ...pageProps }
}

export default Home
